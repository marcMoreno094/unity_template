﻿using UnityEngine;
using System.Collections;
using System.IO;
using SQLite4Unity3d;

public class DB : MonoBehaviour {

	// Use this for initialization
	void Start () {
        //Debug.Log(Application.persistentDataPath);
        string dbPath = Application.persistentDataPath + "/DemoDB";
	    if(!File.Exists(dbPath))
        {
            Debug.Log("Database not found, copying from 'streamingAssets' folder");
            File.Copy(Application.streamingAssetsPath + "/DemoDB", dbPath, false);
            Debug.Log("Copied successfuly to " + dbPath);
        }

        

        var _connection = new SQLiteConnection(dbPath, SQLiteOpenFlags.ReadWrite);

        var highscores = _connection.Table<usersAlternative>();
        
        var test = new usersAlternative();
        test.name = "pepe";
        test.highscore = 1185;

        _connection.Insert(test);

        foreach (usersAlternative u in highscores)
        {
            Debug.Log(u.ToString());
        }

        //var ds = new DataService("DemoDB");
        


	}
}
